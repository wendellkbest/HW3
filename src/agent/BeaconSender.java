package agent;

import manager.BeaconListener;

import java.rmi.Naming;

public class BeaconSender extends Thread{
    private Beacon beacon;
    private String managerIP;

    public BeaconSender(Beacon beacon, String managerip){
        this.beacon = beacon;
        managerIP=managerip;
    }

    public void run(){
        int counter = 0, sleepTime = 1000;
        while(true){
           try{
               BeaconListener access = (BeaconListener) Naming.lookup("rmi://"+managerIP+":1900"+"/BeaconListener");
               access.deposit(beacon);
               System.out.println("Sent beacon ID: "+beacon.getBeaconID());
               Thread.sleep(sleepTime);
           }catch(Exception e){e.printStackTrace();}
            counter++;
           if(counter == 2){
               System.out.println("Delaying beacon interval to four seconds for 8 seconds");
               sleepTime = 4000;
           }
           else if(counter == 4){
               sleepTime = 1000;
               counter = 0;
           }
        }
    }
}
