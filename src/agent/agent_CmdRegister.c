#include <stdio.h>
#include <jni.h>
#include <time.h>
#include "agent_CmdRegister.h"

JNIEXPORT void JNICALL Java_agent_CmdRegister_C_1GetLocalTime
  (JNIEnv *env, jobject obj, jobject alpha){
    //printf("Entered the C code GetLocalTime\n");
    jclass cls = (*env)->GetObjectClass(env, alpha);
    jmethodID mid1 = (*env)->GetMethodID(env, cls, "setTime", "(I)V");
    jmethodID mid2 = (*env)->GetMethodID(env, cls, "setValid","(C)V");
    (*env)->CallVoidMethod(env, alpha, mid1, time(0));
    (*env)->CallVoidMethod(env, alpha, mid2, '1');
  }

JNIEXPORT void JNICALL Java_agent_CmdRegister_C_1GetVersion
  (JNIEnv *env, jobject obj, jobject beta){
    jclass cls = (*env)->GetObjectClass(env, beta);
    jmethodID mid = (*env)->GetMethodID(env, cls, "setVersion", "(I)V");
    (*env)->CallVoidMethod(env, beta, mid, 123);
  }