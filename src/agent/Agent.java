package agent;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.time.Instant;
import java.util.Random;

import static java.lang.System.exit;

public class Agent {
    public static void main(String[] args){
        //Run with: java Agent <agent IP> <manager IP>
        if(args.length <2){
            System.err.println("Please include CmdAgent address and Manager address in this order.");
            exit(1);
        }
        Random r = new Random();
        int randomID = r.nextInt(65535-1023) + 1023;
        Instant cur = Instant.now();
        int times = (int)cur.getEpochSecond() / 1000;
        Beacon beacon = new Beacon(randomID, times,"CmdAgent", args[0]);

        BeaconSender bcnsender = new BeaconSender(beacon, args[1]);
        bcnsender.start();

        try{
            CmdAgent obj = new CmdRegister();
            LocateRegistry.createRegistry(beacon.getBeaconID());
            String argument = "rmi://"+args[0]+":"+beacon.getBeaconID()+"/"+beacon.getCmdAgentID();
            System.out.println("Argument from Agent: "+ argument);
            Naming.rebind(argument,obj);
            System.out.println("CmdRegister Ready");
        }catch(Exception e){e.printStackTrace();}
    }
}
