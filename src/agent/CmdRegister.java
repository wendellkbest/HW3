package agent;

import manager.GetLocalTime;
import manager.GetVersion;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CmdRegister extends UnicastRemoteObject implements CmdAgent{

    public CmdRegister() throws RemoteException {
        super();
    }
    public native void C_GetLocalTime(GetLocalTime alpha);
    public native void C_GetVersion(GetVersion beta);
    static{System.loadLibrary("time");}
    @Override
    public Object execute(String CmdID, Object CmdObject) throws RemoteException {
        System.out.println("Entered C_GetLocalTime1");
        if(CmdID.equals("GetLocalTime")){
            System.out.println("Entered C_GetLocalTime2");
            C_GetLocalTime((GetLocalTime) CmdObject);
            return (GetLocalTime)CmdObject;
        }
        else if(CmdID.equals("GetVersion")){
            System.out.println("Entered C_GetLocalTime3");
            C_GetVersion((GetVersion) CmdObject);
            return (GetVersion)CmdObject;
        }
        return null;
    }
}
