package agent;

import java.io.Serializable;

public class Beacon implements Serializable {
    public static final long serialVersionUID = 1L;
    int ID;
    int StartUpTime;
    String CmdAgentID;
    String agentIP;

    public Beacon(int id, int starttime, String amdagentid, String agentip){
        ID=id;
        StartUpTime=starttime;
        CmdAgentID=amdagentid;
        agentIP=agentip;
    }
    public int getBeaconID(){return ID;}
    public String getCmdAgentID(){return CmdAgentID;}
    public String getAgentIP(){return agentIP;}
}
