package agent;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CmdAgent extends Remote {
    public Object execute(String CmdID, Object CmdObject) throws RemoteException;
}
