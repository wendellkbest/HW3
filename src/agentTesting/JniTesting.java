package agentTesting;

import agent.CmdRegister;
import manager.GetLocalTime;
import manager.GetVersion;

import java.rmi.RemoteException;

public class JniTesting {
    public static void main(String[] args) throws RemoteException {
        CmdRegister test = new CmdRegister();
        GetVersion version = new GetVersion();
        GetLocalTime time = new GetLocalTime();

        test.execute("GetLocalTime", time);
        System.out.println("Time is: " + time.time + " Valid is: "+time.valid);
        test.execute("GetVersion", version);
        System.out.println("Version is: "+version.version);
    }
}
