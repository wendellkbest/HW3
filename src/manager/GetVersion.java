package manager;

import java.io.Serializable;

public class GetVersion implements Serializable {
    public static final long serialVersionUID = 1L;
    public int version;
    public GetVersion(){}

    public void setVersion(int i){version = i;}
}
