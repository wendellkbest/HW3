package manager;

import java.time.LocalTime;
import java.util.Iterator;
import java.util.Set;

import static java.time.temporal.ChronoUnit.SECONDS;

public class Monitor extends Thread{
    public Monitor(){}
    public void run(){
        while(true){
            LocalTime var = LocalTime.now();
            Set<Integer> keys = Manager.beaconlist.keySet();
            Iterator<Integer> iterator = keys.iterator();
            while(iterator.hasNext()){
                int beaconid = iterator.next();
                LocalTime value = Manager.beaconlist.get(beaconid);
                LocalTime plus2val = value.plus(3,SECONDS);
                if(plus2val.compareTo(var)<0){
                    System.out.println("Have not seen beacon ID: "+ beaconid+" In over 3 second");
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
