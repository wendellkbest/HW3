package manager;

import agent.Beacon;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class BeaconListenerRegister extends UnicastRemoteObject implements BeaconListener{

    protected BeaconListenerRegister() throws RemoteException {
    }

    @Override
    public int deposit(Beacon b) throws RemoteException {
        Manager.addBeacon(b);
        return 0;
    }
}
