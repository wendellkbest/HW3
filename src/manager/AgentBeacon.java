package manager;

import agent.Beacon;

import java.time.LocalTime;

public class AgentBeacon {
    private Beacon agentbeacon;
    private LocalTime timereceived;
    private int agentversion;
    private boolean isitnew;
    private boolean lostcontact;

    AgentBeacon(Beacon b, LocalTime time, int agentversion, boolean isitnew){
        agentbeacon = b;
        timereceived = time;
        this.agentversion = agentversion;
        this.isitnew = isitnew;
        lostcontact = false;
    }

    public Beacon getBeacon(){return agentbeacon;}
    public LocalTime getTimereceived(){return timereceived;}
    public boolean getIsitnew(){return isitnew;}
    public int getAgentversion(){return agentversion;}
    public void setAgentversion(int ver){agentversion = ver;}
    public void setIsitnew(boolean tf){isitnew = tf;}
    public void setLostcontact(Boolean value){lostcontact = value;}
    public boolean getLostcontact(){return lostcontact;}
    public void setTimereceived(LocalTime newTime){timereceived = newTime;}
}
