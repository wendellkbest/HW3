package manager;

import agent.Beacon;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.time.LocalTime;
import java.util.HashMap;

public class Manager {
    static HashMap<Integer, LocalTime> beaconlist = new HashMap<>();
    public static void addBeacon(Beacon b) {
        if(beaconlist.isEmpty()){
            beaconlist.put(b.getBeaconID(),LocalTime.now());
            Command getbeaconinfo = new Command(b);
            getbeaconinfo.start();
        }
        else{
            if(beaconlist.containsKey(b.getBeaconID())){
                beaconlist.replace(b.getBeaconID(),LocalTime.now());
            }
            else{
                beaconlist.put(b.getBeaconID(),LocalTime.now());
                Command getbeaconinfo = new Command(b);
                getbeaconinfo.start();
            }
        }
    }
    public static void main(String[] args){
        Monitor monitor = new Monitor();
        monitor.start();
        try{
            BeaconListener obj =new BeaconListenerRegister();
            LocateRegistry.createRegistry(1900);
            Naming.rebind("rmi://localhost:1900"+"/BeaconListener", obj);
            System.out.println("BeaconListenerRegister started");
        }catch(Exception e){e.printStackTrace();}
    }
}
