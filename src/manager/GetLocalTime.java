package manager;

import java.io.Serializable;

public class GetLocalTime implements Serializable {
    public static final long serialVersionUID = 1L;
    public int time;
    public char valid;

    public GetLocalTime(){}
    public void setTime(int newtime){time = newtime;}
    public void setValid(char c){valid = c;}
}
