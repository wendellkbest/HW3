package manager;

import agent.Beacon;
import agent.CmdAgent;

import java.rmi.Naming;

public class Command extends Thread{
    private Beacon beacon;
    public Command(Beacon b){
        this.beacon = b;
    }
    public void run(){
        GetLocalTime glt = new GetLocalTime();
        GetVersion gv = new GetVersion();
        try{
            String argument = "rmi://"+beacon.getAgentIP()+":"+beacon.getBeaconID()+"/"+beacon.getCmdAgentID();
            System.out.println("Command argument: "+argument);
            CmdAgent access = (CmdAgent) Naming.lookup(argument);
            glt=(GetLocalTime)access.execute("GetLocalTime", glt);
            gv=(GetVersion) access.execute("GetVersion", gv);
            System.out.println("Agent local time: "+glt.time+" Valid: "+glt.valid);
            System.out.println(("Version: "+gv.version));
        }catch(Exception e){e.printStackTrace();}
    }
}
